
    <!-- START: Scripts -->

    <!-- GSAP -->
    <script src="<?php echo get_template_directory_uri(); ?>/vender/umbrella/bower_components/gsap/src/minified/TweenMax.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/vender/umbrella/bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

    <!-- History.js -->
    <script src="<?php echo get_template_directory_uri(); ?>/vender/umbrella/bower_components/history.js/scripts/bundled/html4%2Bhtml5/jquery.history.js"></script>

    <!-- Bootstrap Custom -->
    <script src="<?php echo get_template_directory_uri(); ?>/vender/umbrella/js/bootstrap-custom.min.js"></script>

    <!-- Jquery Form -->
    <script src="<?php echo get_template_directory_uri(); ?>/vender/umbrella/bower_components/jquery-form/jquery.form.js"></script>

    <!-- Jquery Validation -->
    <script src="<?php echo get_template_directory_uri(); ?>/vender/umbrella/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>

    <!-- Hammer.js -->
    <script src="<?php echo get_template_directory_uri(); ?>/vender/umbrella/bower_components/hammer.js/hammer.min.js"></script>

    <!-- NanoSroller -->
    <script src="<?php echo get_template_directory_uri(); ?>/vender/umbrella/bower_components/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js"></script>

    <!-- Background Check Custom -->
    <script src="<?php echo get_template_directory_uri(); ?>/vender/umbrella/plugins/background-check/background-check.min.js"></script>

    <!-- Jarallax Video -->
    <script src="<?php echo get_template_directory_uri(); ?>/vender/umbrella/bower_components/jarallax/dist/jarallax-video.min.js"></script>

    <!-- Umbrella -->
    <script src="<?php echo get_template_directory_uri(); ?>/vender/umbrella/js/umbrella.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/vender/umbrella/js/custom.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/vender/umbrella/js/umbrella-init.js"></script>
    <!-- END: Scripts -->



</body>

</html>
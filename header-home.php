<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>

    <!-- START: Styles -->

    
    <!-- Bootstrap Custom -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/vender/umbrella/css/bootstrap-custom.min.css">

    <!-- Umbrella -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/vender/umbrella/css/umbrella.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/vender/umbrella/css/custom.css">

   

    <!-- END: Styles -->

    <!-- jQuery -->
    <script src="<?php echo get_template_directory_uri(); ?>/vender/umbrella/bower_components/jquery/dist/jquery.min.js"></script>

</head>
<body <?php body_class(); ?> <?php design_bank_body_attributes(); ?>>
<?php do_action( 'wp_body_open' ); ?>